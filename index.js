//   Nhập vào một số. In ra số nguyên tố nhỏ hơn hoặc bằng người dùng nhập
document.getElementById("btnKiemTra0").onclick = function () {
  var number = document.getElementById("nhapSo_8").value * 1;
  var ketQua = "";

  for (var i = 2; i <= number; i++) {
    var checkSNT = kiemTraSoNguyenTo(i);
    if (checkSNT) {
      ketQua += i + " ";
    }
  }
  document.getElementById("ketQua8").innerHTML = ketQua;
};

function kiemTraSoNguyenTo(so) {
  var checkSNT = true;
  for (var i = 2; i <= Math.sqrt(so); i++) {
    if (so % i == 0) {
      checkSNT = false;
      break;
    }
  }
  return checkSNT;
}

/* input: nhập vào một số n=11

step:
s1: Tạo một function
s2: Tạo vòng lặp 
s3: Xác định số nào là số nguyên tố từ 1 đến  n = 11 là : 2 3 5 7 11;
s4: Số nào là số nguyên tố thì
output: in ra: số nguyên tố nhỏ hơn hoặc bằng 
*/
